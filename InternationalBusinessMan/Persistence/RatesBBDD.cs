﻿using InternationalBusinessMan.LogError;
using InternationalBusinessMan.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;

namespace InternationalBusinessMan.Persistence
{
    public class RatesBBDD
    {
        static RatesBBDD()
        {
            try { 
                using (var webClient = new WebClient())
                {
                    string rawData = webClient.DownloadString("http://quiet-stone-2094.herokuapp.com/rates.json");
                    rawData = "{\"rates\":" + rawData + "}";
                    try
                    {
                        RateCollection rateCollection = JsonConvert.DeserializeObject<RateCollection>(rawData);
                        rates = rateCollection.Rates;
                    }
                    catch (JsonException j)
                    {
                        ELog.SaveException("JsonException", j);
                    }
                }
            }
            catch(WebException w)
            {
                ELog.SaveException("WebException", w);
            }
            catch (ArgumentException a)
            {
                ELog.SaveException("ArgumentException", a);
            }
            catch (NotSupportedException nt)
            {
                ELog.SaveException("NotSupportedException", nt);
            }
        }

        private static List<RateClass> rates;

        public List<RateClass> Rates { get => rates; set => rates = value; }

    }
}