﻿using InternationalBusinessMan.LogError;
using InternationalBusinessMan.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace InternationalBusinessMan.Persistence
{
    public class TransactionBBDD
    {
        static TransactionBBDD()
        {
            try 
            { 
                using (var webClient = new WebClient())
                {
                    string rawData = webClient.DownloadString("http://quiet-stone-2094.herokuapp.com/transactions.json");
                    rawData = "{\"transactions\":" + rawData + "}";
                    try 
                    {
                        TransactionCollection transactionCollection = JsonConvert.DeserializeObject<TransactionCollection>(rawData);
                        transactions = transactionCollection.Transactions;
                    }
                    catch (JsonException j)
                    {
                        ELog.SaveException("JsonException", j);
                    }
                }
            }
            catch (WebException w)
            {
                ELog.SaveException("WebException", w);
            }
            catch (ArgumentException a)
            {
                ELog.SaveException("ArgumentException", a);
            }
            catch (NotSupportedException nt)
            {
                ELog.SaveException("NotSupportedException", nt);
            }
        }

        private static List<Transaction> transactions;

        public List<Transaction> Transactions { get => transactions; set => transactions = value; }
    }
}