﻿using InternationalBusinessMan.Controller;
using System.Web.Script.Services;
using Newtonsoft.Json;
using System.Web.Services;
using InternationalBusinessMan.LogError;
using InternationalBusinessMan.Model;
using System.Collections.Generic;

namespace InternationalBusinessMan
{
    /// <summary>
    /// Descripción breve de Transaction_Control
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    [System.Web.Script.Services.ScriptService]
    public class Transaction_Control : System.Web.Services.WebService
    {
        TransactionController transactionControl = new TransactionController();
        
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ShowAllRates()
        {
            try 
            {
                string result = JsonConvert.SerializeObject(transactionControl.RatesBBDD.Rates);
                if (result != "null")
                    return result;
                else
                    return "ERROR EN OBTENCIÓN DE DATOS. Consulte con Departamente de IT";
            }
            catch(JsonException j)
            {
                ELog.SaveException("JsonException", j);
                return "ERROR DE SISTEMA. Consulte con Departamente de IT";
            }
            }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ShowAllTransaction()
        {
            try {
                string result = JsonConvert.SerializeObject(transactionControl.TransactionsBBDD.Transactions);
                if (result != "null")
                    return result;
                else
                    return "ERROR EN OBTENCIÓN DE DATOS. Consulte con Departamente de IT";
            }
            catch(JsonException j)
            {
                ELog.SaveException("JsonException", j);
                return "ERROR DE SISTEMA. Consulte con Departamente de IT";
            }
}

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ShowAnSku(string sku)
        {
            try 
            {
                decimal total = 0;
                List<Transaction> result = transactionControl.GetTransactionsFromASku(sku);
                total = transactionControl.GetTotalAmount(result);
                return JsonConvert.SerializeObject(result) + "   Total en Euros: " + total;
            }
            catch(JsonException j)
            {
                ELog.SaveException("JsonException", j);
                return "ERROR DE SISTEMA. Consulte con Departamente de IT";
            }
        }

    }
}
