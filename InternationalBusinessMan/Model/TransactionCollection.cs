﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InternationalBusinessMan.Model
{
    public class TransactionCollection
    {

        private List<Transaction> transactions;

        public List<Transaction> Transactions { get => transactions; set => transactions = value; }
    }
}