﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InternationalBusinessMan.Model
{
    public class Transaction
    {
        private string sku;
        private decimal amount;
        private string currency;

        public string Sku { get => sku; set => sku = value; }
        public decimal Amount { get => amount; set => amount = value; }
        public string Currency { get => currency; set => currency = value; }
    }
}