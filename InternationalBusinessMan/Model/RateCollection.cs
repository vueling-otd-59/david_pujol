﻿using System.Collections.Generic;


namespace InternationalBusinessMan.Model
{
    public class RateCollection
    {

        private List<RateClass> rates;

        public List<RateClass> Rates { get => rates; set => rates = value; }

    }
}