﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InternationalBusinessMan.Model
{
    public class RateClass
    {
        private string from;
        private string to;
        private string rate;

        public string From { get => from; set => from = value; }
        public string To { get => to; set => to = value; }
        public string Rate { get => rate; set => rate = value; }
    }
}