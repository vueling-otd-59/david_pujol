﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using InternationalBusinessMan.Persistence;
using InternationalBusinessMan.Model;

namespace InternationalBusinessMan.Controller
{
    public class TransactionController
    {

        #region Variables
        private string sku;
        private RatesBBDD ratesBBDD;
        private TransactionBBDD transactionsBBDD;

        private const string OFFICIAL_CURRENCY = "EUR";


        public TransactionController()
        {
            InitializeData();
        }

        public string Sku { get => sku; set => sku = value; }
        public RatesBBDD RatesBBDD { get => ratesBBDD; set => ratesBBDD = value; }
        public TransactionBBDD TransactionsBBDD { get => transactionsBBDD; set => transactionsBBDD = value; }
        #endregion

        #region Private Methods
        private void InitializeData()
        {
            ratesBBDD = new RatesBBDD();
            transactionsBBDD = new TransactionBBDD();
        }

        private decimal ExchangeCurrency(string currency, decimal amount)
        {
            decimal exchange = 0;
            bool findExchange = false;
            List<RateClass> rateCurrency = new List<RateClass>();

            foreach (RateClass rate in RatesBBDD.Rates)
            {
                if (!findExchange)
                    if (rate.From == currency && rate.To == OFFICIAL_CURRENCY)
                    {
                        exchange = amount * (Convert.ToDecimal(rate.Rate)/100);
                        findExchange = true;
                        break;
                    }
                    else 
                        if (rate.From == currency)
                            rateCurrency.Add(rate);
            }
            if(!findExchange)
                foreach (RateClass rate in rateCurrency)
                {
                    if (!findExchange)
                    {
                        decimal previousExchange = amount * (Convert.ToDecimal(rate.Rate) / 100);
                        exchange = ExchangeCurrency(rate.To, previousExchange);
                        findExchange = true;
                    }
                }
            return exchange;
        }

        #endregion

        #region Public Methods
        public List<Transaction> GetTransactionsFromASku(string sku)
        {
            List<Transaction> transactionsSku = new List<Transaction>();

            foreach (Transaction transaction in TransactionsBBDD.Transactions)
            {
                if (transaction.Sku.Equals(sku))
                    transactionsSku.Add(transaction);
            }

            foreach(Transaction transaction in transactionsSku)
            {
                if (transaction.Currency != OFFICIAL_CURRENCY) 
                {
                    transaction.Amount = Math.Round((ExchangeCurrency(transaction.Currency, transaction.Amount)), 2, MidpointRounding.ToEven);
                    transaction.Currency = OFFICIAL_CURRENCY;
                }
                    
            }

            return transactionsSku;
        }
        public decimal GetTotalAmount(List<Transaction> transactions)
        {
            decimal total = 0;

            foreach (Transaction trans in transactions)
            {
                total += trans.Amount;
            }

            return total;
        }
        #endregion
    }
}